## Теоретичне питання

Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

Конструкція try...catch використовується для обробки винятків (exceptions) у програмі.

Коли варто використовувати try...catch:

1. Робота з файлами: При читанні або записі в файлах можуть виникати помилки, наприклад, файл може бути відсутнім або відкритим для запису іншою програмою. Використання try...catch дозволяє обробити винятки, що стосуються роботи з файлами і виконати відповідні дії, наприклад, повідомити користувачеві про помилку або вжити запобіжних заходів.

2. Робота з мережевими запитами: При взаємодії з мережевими ресурсами, такими як веб-сервери або бази даних, можуть виникати помилки підключення, таймаути, некоректні відповіді тощо. За допомогою try...catch можна обробити ці помилки та вжити відповідних заходів для відновлення або виводу повідомлень про помилку.

3. Робота зі сторонніми бібліотеками: При використанні сторонніх бібліотек або модулів, можуть виникати помилки, пов'язані з некоректними аргументами, невідповідними типами даних або непередбаченими ситуаціями. Використання try...catch дозволяє перехопити винятки, що виникають у бібліотеці, та вжити відповідні заходи, наприклад, вивести повідомлення про помилку або замінити значення за замовчуванням.

try...catch допомагає керувати виключними ситуаціями, що можуть виникати в програмі, і виконувати необхідні дії для їх обробки.
