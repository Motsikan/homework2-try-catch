const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];


function createListOfBooks(arr, ...properties) {
  const list = document.createElement("ul");
  arr.forEach((book, index) => {
    try {
      properties.forEach((property) => {
        if (!book[property]) {
          throw new Error(`The book №${index + 1} does not have ${property}`);
          //  throw new Error(
          //    `The book Name: "${book.name}" does not have ${property}`
          //  );
        }
      });
      const item = document.createElement("li");
      item.innerHTML = `Author: ${book.author} <br> Name: "${book.name}" <br> Price: ${book.price}$`;
      list.append(item);
    } catch (error) {
      console.error(error.message);
    }
  });
  return list;
}

const booksList = createListOfBooks(books, "author", "name", "price");
const div = document.querySelector("#root");
div.append(booksList);

console.log(div);